package com.HW3;

import java.util.Scanner;

public class MyStrings {
    public static void main(String[] args) {}


    //1
    public  char[] showMeNormalLowAlphabet() {
        int size = 91 - 65; //90- юникод буквы Z, 65 - юникод А.
        char[] array = new char[size];
        int index = 0;
        for (int i = 65; i <= 90; i++) {
            array[index++] = (char) i;
        }
        return array;
    }
    public  char[] showMeReverseAlphabet() {
        int size = 123 - 97; //122- юникод буквы z, 97 - юникод a.
        char[] array = new char[size];
        int index = 0;
        for (int i = 122; i >= 97; i--) {
            array[index++] = (char) i;
        }
        return array;
    }
    public  char[] showMeRusLowAlphabet() {
        int size = 1104 - 1072; //1103 - юникод буквы я, 1072- юникод буквы а.
        char[] array = new char[size];
        int index = 0;
        for (int i = 1072; i <= 1103; i++) {
            array[index++] = (char) i;
        }
        return array;
    }
    public  char[] showMeNumbs() {
        int size = 58 - 48; //57 - юникод  9, 48- юникод 0.
        char[] array = new char[size];
        int index = 0;
        for (int i = 48; i <= 57; i++) {
            array[index++] = (char) i;
        }
        return array;
    }
    public  char[] showMeSymbols() {
        int size = 127 - 32; //126 - юникод  ~, 32- юникод пробела.
        char[] array = new char[size];
        int index = 0;
        for (int i = 32; i <= 126; i++) {
            array[index++] = (char) i;
        }
        return array;
    }

    //2
    public  String changeNumToString(int num) {
        String result = "";

        String first = num + ""; // конкатенация с пустой строкой
        String second = String.valueOf(num); // вызов статического метода valueOf класса String
        String third = Integer.toString(num); // вызов метода toString класса-обертки
        result = third;

        return result;
    }
    public  String changeDoubleNumToString(double num) {
        String result = "";

        String first = num + ""; // конкатенация с пустой строкой
        String second = String.valueOf(num); // вызов статического метода valueOf класса String
        String third = Double.toString(num); // вызов метода toString класса-обертки
        result = third;

        return result;
    }
    public  int changeStringToNum(String number) {
        int result;

        int first = Integer.parseInt(number);

        //получаем примитивный тип (primitive type)
        //используя метод parseXхх нужного класса-обертки,
        //где Xxx - имя примитива с заглавной буквы (например parseInt)
        int second = Integer.valueOf(number); // получаем объект wrapper класса и автоматически распаковываем
        result = second;
        return result;
    }
    public  double changeStringToDoubleNum(String number) {
        double result;

        double first = Double.parseDouble(number);

        //получаем примитивный тип (primitive type)
        //используя метод parseXхх нужного класса-обертки,
        //где Xxx - имя примитива с заглавной буквы (например parseInt)
        double second = Double.valueOf(number); // получаем объект wrapper класса и автоматически распаковываем
        result = second;
        return result;


    }

    //3
    public  int findLengthOfSmallestWord(String myString) {
        int result = 0;

        String[] splitString = myString.split(" "); //splitString = myString.split(" ");

        String firstString = splitString[0];
        String secondString = splitString[1];
        String thirdString = splitString[2];
        String fourthString = splitString[3];

        int a = firstString.length();
        int b = secondString.length();
        int c = thirdString.length();
        int d = fourthString.length();

        int[] myNumsArray = new int[splitString.length]; //[5 2 4  6 ]
        myNumsArray[0] = a; //5
        myNumsArray[1] = b; //2
        myNumsArray[2] = c; //4
        myNumsArray[3] = d; //6


        int minValue = myNumsArray[0];

        for (int i = 0; i < myNumsArray.length; i++) {
            if (myNumsArray[i] < minValue) {
                minValue = myNumsArray[i];

            }
            result = minValue;
        }
        return result;


    }
    public  String[] changeLastThreeSymbols(String[] myStrings, int lengthDef) {
        String[] resultArray;
        resultArray = new String[myStrings.length]; // создаем итоговый массив такой же длинны как заданный
        int index = 0; // индекс - это индексы нашего итогового ассива
        for (int i = 0; i < myStrings.length; i++) { //проходим по данному масиву по всем элементам
            if (myStrings[i].length() == lengthDef) { // сравниваем слова по длинне с деф. значением
                StringBuilder sb = new StringBuilder(myStrings[i]); // если слово подходит по длинне переводим его в стрингбилдер
                sb.delete(myStrings.length - 3, myStrings.length); // обрезаем его
                sb.append("$$$"); // дописываем нужные знаки
                resultArray[index++] = sb.toString(); //и записываем в результат

            } else { // если длинна отличается
                resultArray[index++] = myStrings[i]; // просто перезаписываем как слово и было
            }
        }
        return resultArray;
    }
    public  String addSpaceBetweenWords(String myString) {
        String result = "";
        char[] punctuationMarks = new char[]{'.', '!', '?', ',', ';', ':', '-'}; //создаем массив значений, которые нужно заменить
        StringBuilder mySB = new StringBuilder(myString); //стрингбилдер, чтоб потом использовать вставку (инсерт)
        for (int i = 0; i < mySB.length() - 1; i++) { //идем по каждому слову, кроме последнего
            for (char ch : punctuationMarks) { // for each присвой ch значение каждого элем. массива punctuationMarks по очереди.
                if (mySB.charAt(i) == ch) { // если эл в строке равен какому-то имволу, то проверь еще одно условие
                    if (!mySB.substring(i + 1, i + 2).equals(" ")) { // если вырезанный элемент не равен пробелу, то
                        mySB.insert(i + 1, ' '); // то добавь пробел
                    }
                }
            }
        }
        result = mySB.toString();
        return result;
    }
    public  String leaveUniqueSymbol(String myText) {
        String resultString = "";
        for (int i=0; i<myText.length(); i++){
            char symbol=myText.charAt(i);
            if (resultString.indexOf(symbol)==-1){
                resultString +=symbol;
            }else{
                resultString.replace(String.valueOf(symbol), "");
            }
        }
        return resultString;
    }

    public int getWords() {
        Scanner inputSentence = new Scanner(System.in); // сканер для считывания ввода с консоли
        String inputS = inputSentence.nextLine();
        int result = countQuantityOfWords(inputS);
        return result;
    }
    public int countQuantityOfWords(String inputS) { // как проверить метод, если он со скаером??
        int result = 0;
        int count = 0; // количество слов

        if (inputS.length() != 0) {  //если количество слов не равно 0 тогда оно увеличивается
            count++;

            for (int i = 0; i < inputS.length(); i++) { // Создаем цикл, который идет по строке и проверяет каждый символ, не пробел ли это
                if (inputS.charAt(i) == ' ') {
                    count++; //Если символ равен пробелу - увеличиваем количество слов на 1
                }
                result = count;
            }
        }
        return result;
    }

    public  String cutMyString(String myString) { //substring metod

        String result = "";
        myString.length();
        String newString = myString.substring(4, myString.length() - 7);
        result = newString;
        return result;
    }
    public  String reverseMyString(String myString) { //reverse metod
        String result;

        StringBuffer buffer = new StringBuffer(myString);
        buffer.reverse();
        result = buffer.toString();
        return result;
    }
    public  String toDelLastWord(String myString) { // substring metod for String
        String result = myString.substring(0, myString.lastIndexOf(' '));
        return result;

    }
}

