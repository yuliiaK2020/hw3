package com.HW3;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class MyStringsTest {
    private static MyStrings ms;
    @BeforeAll
    static void init() {
        ms = new MyStrings();
    }

//1

   @Test
   public void showMeNormalLowAlphabet_A_Z (){
        String expected = "[A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z]";
        String actual = Arrays.toString(ms.showMeNormalLowAlphabet());
        Assertions.assertEquals(expected, actual);
   }
    @Test
    public void showMeReverseAlphabet_z_a (){
        String expected = "[z, y, x, w, v, u, t, s, r, q, p, o, n, m, l, k, j, i, h, g, f, e, d, c, b, a]";
        String actual = Arrays.toString(ms.showMeReverseAlphabet());
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void showMeRusLowAlphabet_a_ia (){
        String expected = "[а, б, в, г, д, е, ж, з, и, й, к, л, м, н, о, п, р, с, т, у, ф, х, ц, ч, ш, щ, ъ, ы, ь, э, ю, я]";
        String actual = Arrays.toString(ms.showMeRusLowAlphabet());
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void showMeNumbs_0_9 (){
        String expected = "[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]";
        String actual = Arrays.toString(ms.showMeNumbs());
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void showMeSymbols (){
        String expected = "[ , !, \", #, $, %, &, ', (, ), *, +, ,, -, ., /, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, :, ;, <, =, >, ?, @, A, B, C, D, E, F, G, H, I, J, K, L, M, N, O, P, Q, R, S, T, U, V, W, X, Y, Z, [, \\, ], ^, _, `, a, b, c, d, e, f, g, h, i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y, z, {, |, }, ~]";
        String actual = Arrays.toString(ms.showMeSymbols());
        Assertions.assertEquals(expected, actual);
    }

//2

    @Test
    public void changeNumToString_test_0 (){
        String expected = "0";
        String actual = ms.changeNumToString (0);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeNumToString_test_111 (){
        String expected = "111";
        String actual = ms.changeNumToString (111);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeNumToString_test_neg578 (){
        String expected = "-578";
        String actual = ms.changeNumToString (-578);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeDoubleNumToString_test_neg2point5 (){
        String expected = "-2.5";
        String actual = ms.changeDoubleNumToString (-2.5);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeDoubleNumToString_test_0point0 (){
        String expected = "0.0";
        String actual = ms.changeDoubleNumToString (0.0);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeDoubleNumToString_test_211point876 (){
        String expected = "211.876";
        String actual = ms.changeDoubleNumToString (211.876);
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToNum_test_neg892 (){
        int expected = -892;
        int actual = ms.changeStringToNum ("-892");
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToNum_test_0 (){
        int expected = 0;
        int actual = ms.changeStringToNum ("0");
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToNum_test_765 (){
        int expected = 765;
        int actual = ms.changeStringToNum ("765");
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToDoubleNum_test_neg4point11 (){
        double expected = -4.11;
        double actual = ms.changeStringToDoubleNum ("-4.11");
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToDoubleNum_test_0point1 (){
        double expected = 0.1;
        double actual = ms.changeStringToDoubleNum ("0.1");
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void changeStringToDoubleNum_test_451point806 (){
        double expected = 451.806;
        double actual = ms.changeStringToDoubleNum ("451.806");
        Assertions.assertEquals(expected, actual);
    }

//3
    //1
    @Test
     public void findLengthOfSmallestWord_test_hello_my_dear_friend () {
        int expected = 2;
        int actual = ms.findLengthOfSmallestWord("hello my dear friend");
        Assertions.assertEquals(expected, actual);
      }
    //2
     @Test
     public void changeLastThreeSymbols_test_5words_5letters () {
         String expected = "[страна, квартира, дв$$$, ру$$$, ви$$$]";
         String actual = Arrays.toString(ms.changeLastThreeSymbols(new String[]{"страна", "квартира", "двери", "ручка", "вилка"}, 5));
         Assertions.assertEquals(expected, actual);
     }
    //3
    @Test
    public void addSpaceBetweenWords_test_The_sun_is_shining_the_weather_is_good(){
        String expected = "The sun. is shining, the weather: is good!";
        String actual = ms.addSpaceBetweenWords ("The sun.is shining,the weather:is good!");
        Assertions.assertEquals(expected, actual);
    }
    //4
    @Test
    public void leaveUniqueSymbol_test_The_sun_is_shining(){
        String expected = "The sunig";
        String actual = ms.leaveUniqueSymbol("The sun is shining");
              Assertions.assertEquals(expected, actual);
    }

    //5
    @Test
    public void countQuantityOfWords_check (){
        int expected = 4;
        int actual = ms.countQuantityOfWords("The sun is shining");
        Assertions.assertEquals(expected, actual);
    }
    //6
    @Test
    public void cutMyString_test_The_sun_is_shining (){
        String expected = "sun is ";
        String actual = ms.cutMyString ("The sun is shining");
        Assertions.assertEquals(expected, actual);
    }
    // 7
    @Test
    public void reverseMyString_test_The_sun_is_shining (){
        String expected = "gninihs si nus ehT";
        String actual = ms.reverseMyString ("The sun is shining");
        Assertions.assertEquals(expected, actual);
    }
    //8
    @Test
    public void toDelLastWord_test_The_sun_is_shining (){
        String expected = "The sun is";
        String actual = ms.toDelLastWord ("The sun is shining");
        Assertions.assertEquals(expected, actual);
    }

}
